﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AntibodySpawner : MonoBehaviour
{
    public GameObject SpawnEffect;
    public GameObject BiocodeLauncher;
    public GameObject AntibodyPrefab;
    public GameObject AntibodyPrefab2;
    public GameObject AntibodyPrefab3;
    public GameObject CoinPrefab;

    public int ABSpawned = 0;
    public static bool LauncherSpawned = false;

    private float x, y, z;

    public bool spawn1 = false;
    public bool spawn2 = false;
    public bool spawn3 = false;
    public bool spawn4 = false;

    public bool CanSet = false;

    public void SetABSP()
    {
        if (CanSet)
        {
            if (ABSpawned == 3) ABSpawned = 2;
            if (ABSpawned == 2) ABSpawned = 1;
            if (ABSpawned == 1) ABSpawned = 0;
        }
        CanSet = false;
    }

    IEnumerator SpawnLauncher()
    {
        Vector3 pos = new Vector3(0, 3, 0);
        //this.GetComponent<AudioSource>().Play();
        Instantiate(SpawnEffect, pos, Quaternion.identity);
        yield return new WaitForSeconds(1.4f);
        BiocodeLauncher.SetActive(true);
        Destroy(GameObject.Find("Particle revolver 001 Megido(Clone)"));
        yield return null;
    }

    private void Start()
    {
        Time.timeScale = 1;
        LauncherSpawned = BiocodeLauncher.activeInHierarchy;

        if (!LauncherSpawned)
        {
            StartCoroutine(SpawnLauncher());
            LauncherSpawned = true;
        }

        if (SceneManager.GetActiveScene().name != "Level1")
        {
            InvokeRepeating("SpawnAntibody", 2.0f, 3.0f);
            InvokeRepeating("SpawnCoin", 2.0f, 10.0f);
        }
    }

    public void SpawnCoin()
    {
        x = 0f; z = 0f;
        y = 3.53f;
        if (!spawn1)
        {
            x = -6.52f;
            z = 12.58f;
            spawn1 = true;
        }
        else if (!spawn2)
        {
            x = 10.88f;
            z = -5.18f;
            spawn2 = true;
        }
        else if (!spawn3)
        {
            x = 4.39f;
            z = -13.33f;
            spawn3 = true;
        }
        else if (!spawn4)
        {
            x = -10.37f;
            z = 1.66f;
            spawn3 = true;
        }

        if (x != 0f) Instantiate(CoinPrefab, new Vector3(x, y, z), Quaternion.identity);
    }

    public void CanSpawnCoinAt(Transform position)
    {
        float x = position.position.x;

        if (x == -6.52f) spawn1 = false;
        else if (x == 10.88f) spawn2 = false;
        else if (x == 4.39f) spawn3 = false;
        else if (x == -10.37f) spawn4 = false;
    }


    public void SpawnAntibody()
    {
        int i = Random.Range(0, 100);

        y = 3.53f;
        if (i % 4 == 0)
        {
            x = 11.49f;
            z = 19f;
        }
        else if (i % 4 == 1)
        {
            x = 19.35f;
            z = 11.49f;
        }
        else if (i % 4 == 2)
        {
            x = -18.52f;
            z = -11.29f;
        }
        else if (i % 4 == 3)
        {
            x = -11.43f;
            z = -18.42f;
        }

        if (SceneManager.GetActiveScene().name == "Level2" || SceneManager.GetActiveScene().name == "Level3" || SceneManager.GetActiveScene().name == "Level4")
        {
            if (ABSpawned < 1)
            {
                Instantiate(AntibodyPrefab, new Vector3(x, y, z), Quaternion.identity);
                ABSpawned++;
            }
        }
        else if (SceneManager.GetActiveScene().name == "Level5" || SceneManager.GetActiveScene().name == "Level6" || SceneManager.GetActiveScene().name == "Level7" || SceneManager.GetActiveScene().name == "Level8")
        {
            if (ABSpawned < 1)
            {
                int which = Random.Range(0, 100);
                if (which % 4 == 2)
                {
                    Instantiate(AntibodyPrefab2, new Vector3(x, y, z), Quaternion.identity);
                    ABSpawned++;
                }
                else
                {
                    Instantiate(AntibodyPrefab, new Vector3(x, y, z), Quaternion.identity);
                    ABSpawned++;
                }
            }
        }
        else if (SceneManager.GetActiveScene().name == "Level9" || SceneManager.GetActiveScene().name == "Level10" || SceneManager.GetActiveScene().name == "Level11" || SceneManager.GetActiveScene().name == "Level12")
        {
            if (ABSpawned < 1)
            {
                int which = Random.Range(0, 100);
                if (which % 10 == 0)
                {
                    Instantiate(AntibodyPrefab3, new Vector3(x, y, z), Quaternion.identity);
                    ABSpawned++;
                }
                else if (which % 10 == 5)
                {
                    Instantiate(AntibodyPrefab2, new Vector3(x, y, z), Quaternion.identity);
                    ABSpawned++;
                }
                else
                {
                    Instantiate(AntibodyPrefab, new Vector3(x, y, z), Quaternion.identity);
                    ABSpawned++;
                }
            }
        }
    }
}
