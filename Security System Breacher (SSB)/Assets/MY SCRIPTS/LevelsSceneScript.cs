﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelsSceneScript : MonoBehaviour
{
    public PlayerData data;
    int DamageCost;
    int PowerCost;

    private void Costs()
    {
        if (data.damage == 1) DamageCost = 20;
        else if (data.damage == 2) DamageCost = 30;
        else if (data.damage == 3) DamageCost = 40;
        else DamageCost = 0;

        if (data.maxhealth == 5) PowerCost = 30;
        else if (data.maxhealth == 10) PowerCost = 40;
        else if (data.maxhealth == 15) PowerCost = 50;
        else PowerCost = 0;
    }

    private void UpdateUI()
    {
        if (!data.level2)
        {
            GameObject.Find("Canvas/Panel/Level2Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level2Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level2Button").GetComponentInChildren<Text>().text = " Level 2";
            GameObject.Find("Canvas/Panel/Level2Button/Diffculty").SetActive(true);
        }

        if (!data.level3)
        {
            GameObject.Find("Canvas/Panel/Level3Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level3Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level3Button").GetComponentInChildren<Text>().text = " Level 3";
            GameObject.Find("Canvas/Panel/Level3Button/Diffculty").SetActive(true);
        }

        if (!data.level4)
        {
            GameObject.Find("Canvas/Panel/Level4Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level4Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level4Button").GetComponentInChildren<Text>().text = " Level 4";
            GameObject.Find("Canvas/Panel/Level4Button/Diffculty").SetActive(true);
        }

        if (!data.level5)
        {
            GameObject.Find("Canvas/Panel/Level5Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level5Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level5Button").GetComponentInChildren<Text>().text = " Level 5";
            GameObject.Find("Canvas/Panel/Level5Button/Diffculty").SetActive(true);
        }

        if (!data.level6)
        {
            GameObject.Find("Canvas/Panel/Level6Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level6Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level6Button").GetComponentInChildren<Text>().text = " Level 6";
            GameObject.Find("Canvas/Panel/Level6Button/Diffculty").SetActive(true);
        }

        if (!data.level7)
        {
            GameObject.Find("Canvas/Panel/Level7Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level7Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level7Button").GetComponentInChildren<Text>().text = " Level 7";
            GameObject.Find("Canvas/Panel/Level7Button/Diffculty").SetActive(true);
        }

        if (!data.level8)
        {
            GameObject.Find("Canvas/Panel/Level8Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level8Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level8Button").GetComponentInChildren<Text>().text = " Level 8";
            GameObject.Find("Canvas/Panel/Level8Button/Diffculty").SetActive(true);
        }

        if (!data.level9)
        {
            GameObject.Find("Canvas/Panel/Level9Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level9Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level9Button").GetComponentInChildren<Text>().text = " Level 9";
            GameObject.Find("Canvas/Panel/Level9Button/Diffculty").SetActive(true);
        }

        if (!data.level10)
        {
            GameObject.Find("Canvas/Panel/Level10Button").GetComponentInChildren<Text>().text = "  Locked";
            GameObject.Find("Canvas/Panel/Level10Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level10Button").GetComponentInChildren<Text>().text = " Level 10";
            GameObject.Find("Canvas/Panel/Level10Button/Diffculty").SetActive(true);
        }

        if (!data.level11)
        {
            GameObject.Find("Canvas/Panel/Level11Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level11Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level11Button").GetComponentInChildren<Text>().text = " Level 11";
            GameObject.Find("Canvas/Panel/Level11Button/Diffculty").SetActive(true);
        }

        if (!data.level12)
        {
            GameObject.Find("Canvas/Panel/Level12Button").GetComponentInChildren<Text>().text = "  Locked.";
            GameObject.Find("Canvas/Panel/Level12Button/Diffculty").SetActive(false);
        }
        else
        {
            GameObject.Find("Canvas/Panel/Level12Button").GetComponentInChildren<Text>().text = " Level 12";
            GameObject.Find("Canvas/Panel/Level12Button/Diffculty").SetActive(true);
        }

        GameObject.Find("Canvas/Panel/Shop/Current Power").GetComponent<Text>().text = "Current Power: " + data.maxhealth;
        GameObject.Find("Canvas/Panel/Shop/Current Damage").GetComponent<Text>().text = "Current Damage: " + data.damage;
        GameObject.Find("Canvas/Panel/Shop/CoinsText").GetComponent<Text>().text = "Coins: " + data.coins;

        Costs();

        if (DamageCost != 0) GameObject.Find("Canvas/Panel/Shop/DamageCostText").GetComponent<Text>().text = " " + DamageCost + " coins";
        else GameObject.Find("Canvas/Panel/Shop/DamageCostText").GetComponent<Text>().text = "  Maxed";
        if (PowerCost != 0) GameObject.Find("Canvas/Panel/Shop/PowerCostText").GetComponent<Text>().text = " " + PowerCost + " coins";
        else GameObject.Find("Canvas/Panel/Shop/PowerCostText").GetComponent<Text>().text = "  Maxed";
    }

    private void Start()
    {
        data = SaveSystem.LoadPlayer();
        UpdateUI();
    }

    #region Levels Management

    public void Level1Button_Click()
    {
        SceneManager.LoadScene(3, LoadSceneMode.Single);
    }

    public void Level2Button_Click()
    {
        if (data.level2)
        {
            SceneManager.LoadScene(4, LoadSceneMode.Single);
        }
    }

    public void Level3Button_Click()
    {
        if (data.level3)
        {
            SceneManager.LoadScene(5, LoadSceneMode.Single);
        }
    }

    public void Level4Button_Click()
    {
        if (data.level4)
        {
            SceneManager.LoadScene(6, LoadSceneMode.Single);
        }
    }

    public void Level5Button_Click()
    {
        if (data.level5)
        {
            SceneManager.LoadScene(7, LoadSceneMode.Single);
        }
    }

    public void Level6Button_Click()
    {
        if (data.level6)
        {
            SceneManager.LoadScene(8, LoadSceneMode.Single);
        }
    }

    public void Level7Button_Click()
    {
        if (data.level7)
        {
            SceneManager.LoadScene(9, LoadSceneMode.Single);
        }
    }

    public void Level8Button_Click()
    {
        if (data.level8)
        {
            SceneManager.LoadScene(10, LoadSceneMode.Single);
        }
    }

    public void Level9Button_Click()
    {
        if (data.level9)
        {
            SceneManager.LoadScene(11, LoadSceneMode.Single);
        }
    }

    public void Level10Button_Click()
    {
        if (data.level10)
        {
            SceneManager.LoadScene(12, LoadSceneMode.Single);
        }
    }

    public void Level11Button_Click()
    {
        if (data.level11)
        {
            SceneManager.LoadScene(13, LoadSceneMode.Single);
        }
    }

    public void Level12Button_Click()
    {
        if (data.level12)
        {
            SceneManager.LoadScene(14, LoadSceneMode.Single);
        }
    }

    public void UpgradePowerButton_Click()
    {
        if (PowerCost == 30 && data.coins >= 30)
        {
            data.maxhealth += 5;
            data.coins -= 30;
            PowerCost = 40;
        }
        else if (PowerCost == 40 && data.coins >= 40)
        {
            data.maxhealth += 5;
            data.coins -= 40;
            PowerCost = 50;
        }
        else if (PowerCost == 50 && data.coins >= 50)
        {
            data.maxhealth += 5;
            data.coins -= 50;
            PowerCost = 0;
        }

        UpdateUI();
    }

    public void UpgradeDamageButton_Click()
    {
        if (DamageCost == 20 && data.coins >= 20)
        {
            data.damage++;
            data.coins -= 20;
            DamageCost = 30;
        }
        else if (DamageCost == 30 && data.coins >= 30)
        {
            data.damage++;
            data.coins -= 30;
            DamageCost = 40;
        }
        else if (PowerCost == 40 && data.coins >= 40)
        {
            data.damage++;
            data.coins -= 40;
            DamageCost = 0;
        }

        UpdateUI();
    }

    #endregion

    public void BackButton_Click()
    {
        SceneManager.LoadScene(0);
    }
}
