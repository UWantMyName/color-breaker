﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCoinScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "TumblerBiocode")
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator RotateObject(float angle, Vector3 axis, float inTime)
    {
        // calculate rotation speed
        float rotationSpeed = angle / inTime;

        while (true)
        {
            // save starting rotation position
            Quaternion startRotation = transform.rotation;

            float deltaAngle = 0;

            // rotate until reaching angle
            while (deltaAngle < angle)
            {
                deltaAngle += rotationSpeed * Time.deltaTime;
                deltaAngle = Mathf.Min(deltaAngle, angle);

                transform.rotation = startRotation * Quaternion.AngleAxis(deltaAngle, axis);

                yield return null;
            }
            // delay here
            yield return new WaitForSeconds(0);
        }
    }

    private void Start()
    {
        //StartCoroutine(RotateObject(360f, Vector3.down, 1f));
    }

    private void Update()
    {
        Vector3 curentpos = transform.position;
        transform.RotateAround(curentpos, Vector3.up, 200 * Time.deltaTime);
    }
}
