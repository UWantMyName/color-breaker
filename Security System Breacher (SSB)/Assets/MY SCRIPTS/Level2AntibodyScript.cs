﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level2AntibodyScript : MonoBehaviour
{
    public GameObject TextUI;
    public GameObject ImageUI;

    private string Message = "Watch out for robots. They will slowly approach you and will lower your defenses.";
    public bool announced = false;
    private bool onWrite = false;

    public IEnumerator WriteUI()
    {
        ImageUI.SetActive(true);
        TextUI.SetActive(true);
        TextUI.GetComponent<Text>().text = "";
        foreach (char letter in Message.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }

        yield return new WaitForSeconds(6);

        TextUI.GetComponent<Text>().text = "";
        Message = "Shoot a sphere at the robots to damage them. Be careful, though. They won't take damage when the shields are up.";
        foreach (char letter in Message.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }

        yield return new WaitForSeconds(6);

        ImageUI.SetActive(false);
        TextUI.SetActive(false);
        StopCoroutine(WriteUI());
    }

    private void FixedUpdate()
    {
        if (GameObject.Find("BiocodeLauncherPrefab") != null)
        {
            GameObject AB = GameObject.Find("AntiBody1Prefab(Clone)");
            if (AB != null && AB.GetComponent<Renderer>().isVisible)
            {
                if (!onWrite)
                {
                    onWrite = true;
                    StartCoroutine(WriteUI());
                }
            }

        }
    }
}
