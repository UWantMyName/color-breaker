﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControllerScript : MonoBehaviour
{
    public GameObject AreYouSureUI;
    public GameObject PlayUI;

    public GameObject PlayButton;
    public GameObject CreditsButton;
    public GameObject QuitButton;

    public GameObject CreditsText;
    public GameObject BackButton;


    #region Quit Controller
    public void QuitButton_Click()
    {
        PlayUI.SetActive(false);
        AreYouSureUI.SetActive(true);
    }

    public void QuitButton_Hover()
    {
        GameObject.Find("Camera/HoverSound").GetComponent<AudioSource>().Play();
    }

    public void YesButton_Click()
    {
        Application.Quit();
    }

    public void NoButton_Click()
    {
        AreYouSureUI.SetActive(false);
    }
    #endregion

    public void Hover()
    {
        GameObject.Find("Camera/HoverSound").GetComponent<AudioSource>().Play();
    }

    #region Play Controller
    public void PlayButton_Click()
    {
        AreYouSureUI.SetActive(false);
        PlayUI.SetActive(true);
        GameObject.Find("Canvas/Panel/PlayUI/NewGameButton").SetActive(true);
        if (File.Exists(Application.persistentDataPath + "/player.zdatao")) GameObject.Find("Canvas/Panel/PlayUI/ContinueButton").SetActive(true);
    }

    public void PlayButton_Hover()
    {
        GameObject.Find("Camera/HoverSound").GetComponent<AudioSource>().Play();
    }
    #endregion

    #region SettingsController

    public void SettingsButton_Hover()
    {
        GameObject.Find("Camera/HoverSound").GetComponent<AudioSource>().Play();
    }

    public void SettingsButton_Click()
    {
        PlayButton.SetActive(false);
        CreditsButton.SetActive(false);
        QuitButton.SetActive(false);

        CreditsText.SetActive(true);
        BackButton.SetActive(true);
    }

    #endregion

    #region New Game Controller
    public void NewGameButton_Click()
    {
        Player newplayer = new Player();
        newplayer = newplayer.CreatePlayer();
        SaveSystem.SavePlayer(newplayer);
        SceneManager.LoadScene(1);
    }

    public void ContinueButton_Click()
    {
        SceneManager.LoadScene(1);
    }
    #endregion

    #region Back Button Controller

    public void BackButton_Hover()
    {
        GameObject.Find("Camera/HoverSound").GetComponent<AudioSource>().Play();
    }

    public void BackButton_Click()
    {
        PlayButton.SetActive(true);
        CreditsButton.SetActive(true);
        QuitButton.SetActive(true);

        CreditsText.SetActive(false);
        BackButton.SetActive(false);
    }

    #endregion
}
