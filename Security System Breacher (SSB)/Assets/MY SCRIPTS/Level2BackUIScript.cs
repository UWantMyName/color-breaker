﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2BackUIScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().enabled = false;
    }

    public void ProceedButton_OnClick()
    {
        GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().enabled = true;
        this.enabled = false;
    }
}
