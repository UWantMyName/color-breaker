﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootBiocodeScript : MonoBehaviour
{
    public GameObject DestroyEffect;

    public Rigidbody rb;
    public float force = 1.0f;

    private Vector3 tr;
    private int distance = 0;

    IEnumerator DestroyBiocode(GameObject me, GameObject WhatIHitObject)
    {
        this.GetComponent<AudioSource>().Play();
        GameObject.Find("BiocodesLeftPrefab").GetComponent<BiocodeCounter>().BiocodesLeft--;
        me.SetActive(false);
        WhatIHitObject.GetComponentInParent<RotateTumblerScript>().BiocodesLeft--;
        Destroy(WhatIHitObject);
        Instantiate(DestroyEffect, WhatIHitObject.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(GameObject.Find("Emitter_For_Instance_OneShoot_B(Clone)"));
        Destroy(me);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject me = rb.gameObject;
        GameObject WhatIHitObject = collision.gameObject;
        string WhatIHit = collision.gameObject.name;

        if (WhatIHitObject.tag == "Biocode" && WhatIHitObject.transform.parent.parent != null)
        {
            if ((WhatIHitObject.GetComponent<Renderer>().sharedMaterial == me.GetComponent<Renderer>().sharedMaterial) || me.GetComponent<Renderer>().sharedMaterial.name == "BiocodeMaterialOmni")
            {
                StartCoroutine(DestroyBiocode(me, WhatIHitObject));
            }
            else
            {
                WhatIHitObject.GetComponentInParent<RotateTumblerScript>().ErrorRotation = true;
                if (SceneManager.GetActiveScene().name == "Level1")
                {
                    if (!GameObject.Find("TutorialUI").GetComponent<WritingAnimationLvl1>().error)
                    {
                        GameObject.Find("TutorialUI").GetComponent<WritingAnimationLvl1>().error = true;
                        GameObject.Find("TutorialUI").GetComponent<WritingAnimationLvl1>().triger2 = true;
                    }
                }
                Destroy(me);
            }
        }
        else if (WhatIHitObject.tag == "Coin")
        {
            Destroy(me);
            GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().CanSpawnCoinAt(WhatIHitObject.transform);
            Destroy(WhatIHitObject);
            GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().coins++;
        }
        else if (WhatIHit == "ShieldPrefab")
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
        }
        else if (WhatIHitObject.GetComponent<Renderer>().sharedMaterial != me.GetComponent<Renderer>().sharedMaterial && WhatIHit != "Shooter" && WhatIHit != "ShieldPrefab")
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
            Destroy(me);
        }
        else Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
    }

    private void Start()
    {
        tr = GameObject.Find("BiocodeLauncherPrefab").transform.forward;
    }

    // Update is called once per frame
    void Update ()
    {
        rb.AddForce(tr * (force ));
        distance+=5;
        if (distance > 350) Destroy(gameObject);
	}
}
