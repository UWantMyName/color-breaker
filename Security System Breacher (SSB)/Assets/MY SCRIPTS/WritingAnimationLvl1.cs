﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WritingAnimationLvl1 : MonoBehaviour
{
    public string Message = "Rotate the launcher by pressing the A/D or left/right arrow keys.";
    string Message2 = " Press space to shoot a colored sphere.";
    string Message22 = " Destroy the spheres by matching them with the same color.";
    string ErrorMess = "If the spheres don't match, the tumbler will spin. Wait for the tumbler to stop so you can fire at it again.";
    public GameObject TextUI;
    public GameObject ImageUI;
    private int TimeLapse;
    private bool started1 = false, triger1 = false;
    public bool triger2 = false;
    public bool error = false;

    public IEnumerator WriteUI()
    {
        ImageUI.SetActive(true);
        TextUI.SetActive(true);
        TextUI.GetComponent<Text>().text = "";
        foreach (char letter in Message.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }
        foreach (char letter in Message2.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }
        foreach (char letter in Message22.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }
        yield return new WaitForSeconds(4);
        ImageUI.SetActive(false);
        TextUI.SetActive(false);
        StopAllCoroutines();
    }

    public IEnumerator ErrorMessage()
    {
        ImageUI.SetActive(true);
        TextUI.SetActive(true);
        TextUI.GetComponent<Text>().text = null;
        foreach (char letter in ErrorMess.ToCharArray())
        {
            TextUI.GetComponent<Text>().text += letter;
            yield return null;
        }
        yield return new WaitForSeconds(4);
        ImageUI.SetActive(false);
        TextUI.SetActive(false);
        StopAllCoroutines();
    }

    private void Update()
    {
        if (GameObject.Find("BiocodeLauncherPrefab") != null)
        {
            if (!started1 && !triger1)
            {
                ImageUI.SetActive(true);
                StopAllCoroutines();
                StartCoroutine(WriteUI());
                started1 = true;
            }
        }

        if (triger2)
        {
            StartCoroutine(ErrorMessage());
            triger2 = false;
        }
    }

}
