﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopScript : MonoBehaviour
{
    public PlayerData data;
    private int DamageCost;
    private int MaxHealthCost;

    private void Start()
    {
        data = SaveSystem.LoadPlayer();
        if (data.damage == 1) GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = "30 coins";
        else if (data.damage == 2) GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = "40 coins";
        else if (data.damage == 3) GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = "50 coins";
        else GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = "Maxed out!";

        if (data.maxhealth == 5) GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = "20 coins";
        else if (data.maxhealth == 10) GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = "30 coins";
        else if (data.maxhealth == 15) GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = "40 coins";
        else GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = "Maxed out!";

        string damageText = GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text;
        string healthText = GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text;

        if (damageText == "30 coins") DamageCost = 30;
        else if (damageText == "40 coins") DamageCost = 40;
        else if (damageText == "50 coins") DamageCost = 50;

        if (healthText == "20 coins") MaxHealthCost = 20;
        else if (healthText == "30 coins") MaxHealthCost = 30;
        else if (healthText == "40 coins") MaxHealthCost = 40;

        data = SaveSystem.LoadPlayer();
        GameObject.Find("Canvas/Panel/Coins").GetComponent<Text>().text = "Coins: " + data.coins;
    }

    private void UpdateScene()
    {
        if (DamageCost != 0) GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = DamageCost + " coins";
        else GameObject.Find("Canvas/Panel/PriceDamage").GetComponent<Text>().text = "Maxed out!";

        if (MaxHealthCost != 0) GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = MaxHealthCost + " coins";
        else GameObject.Find("Canvas/Panel/PriceMaxHealth").GetComponent<Text>().text = "Maxed out!";

        GameObject.Find("Canvas/Panel/Coins").GetComponent<Text>().text = "Coins: " + data.coins;
    }

    public void BuyDamageButton_Click()
    {
        if (data.coins >= DamageCost && DamageCost != 0)
        {
            data.coins -= DamageCost;
            data.damage += 1;
            if (DamageCost == 30) DamageCost = 40;
            else if (DamageCost == 40) DamageCost = 50;
            else if (DamageCost == 50) DamageCost = 0;
            UpdateScene();
        }
    }

    public void BuyMaxHealthButton_Click()
    {
        if (data.coins >= MaxHealthCost && MaxHealthCost != 0)
        {
            data.coins -= MaxHealthCost;
            data.maxhealth += 5;
            if (MaxHealthCost == 20) MaxHealthCost = 30;
            else if (MaxHealthCost == 30) MaxHealthCost = 40;
            else if (MaxHealthCost == 40) MaxHealthCost = 0;
            UpdateScene();
        }
    }

    public void BackButton_Click()
    {
        SaveSystem.SavePlayer(data);
        SceneManager.LoadScene(1);
    }
}
