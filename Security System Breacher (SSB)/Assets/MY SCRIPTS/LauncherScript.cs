﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LauncherScript : MonoBehaviour
{
    private float x;
    private bool Started = false;
    public int coins;

    public int damage;
    public PlayerData data;

    public GameObject LoseMenuUI;
    public GameObject DestroyEffect;

    #region Biocodes
    public GameObject RedBiocodePrefab;
    public GameObject BlueBiocodePrefab;
    public GameObject YellowBiocodePrefab;
    public GameObject GreenBiocodePrefab;
    public GameObject PurpleBiocodePrefab;
    public GameObject OmniBiocodePrefab;
    #endregion

    #region UpdateBiocode
    public GameObject ShootBiocode;
    public GameObject NextBiocode;
    #endregion

    #region UpdateBiocodeVector
    private Vector3 posNext = new Vector3(0.0f, 3.75f, 0.0f);
    #endregion

    #region MiscVariables
    public float RotationSensitivity;
    public int Health = 1;
    #endregion

    IEnumerator DestroyLauncher()
    {
        Instantiate(DestroyEffect, transform.position, transform.rotation);

        yield return new WaitForSeconds(2f);

        StopCoroutine(DestroyLauncher());
        Time.timeScale = 0;
        if (GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned > 0) GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned--;
        Destroy(GameObject.Find("NextBiocodePrefab(Clone)"));
        LoseMenuUI.SetActive(true);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Antibody"))
        {
            Health--;
            StartCoroutine(other.gameObject.GetComponent<AntibodyScript>().KillAntibody());
        }
    }

    private void Start()
    {
        data = SaveSystem.LoadPlayer();
        damage = data.damage;
        Health = data.maxhealth;
        coins = data.coins;
        NextBiocode.transform.position = posNext;
        Instantiate(NextBiocode);
    }

    void Update()
    {
        GameObject.Find("HealthStatus/Panel/HealthBar").GetComponent<RectTransform>().sizeDelta = new Vector2(100 * Health / data.maxhealth, GameObject.Find("HealthStatus/Panel/HealthBar").GetComponent<RectTransform>().sizeDelta.y);
        GameObject.Find("HealthStatus/Panel/CoinsText").GetComponent<Text>().text = "Coins: " + coins.ToString();

        if (Health <= 0)
        {
            if (!Started)
            {
                // I have to save everything to the system
                Player player = new Player();
                player.damage = damage;
                player.coins = coins;
                player.maxhealth = data.maxhealth; // PLAYER DATA! NOT THE ACTUAL PLAYER!!!

                if (SceneManager.GetActiveScene().name == "Level1") player.level2 = true;
                else if (SceneManager.GetActiveScene().name == "Level2") player.level3 = true;
                else if (SceneManager.GetActiveScene().name == "Level3") player.level4 = true;
                else if (SceneManager.GetActiveScene().name == "Level4") player.level5 = true;
                else if (SceneManager.GetActiveScene().name == "Level5") player.level6 = true;
                else if (SceneManager.GetActiveScene().name == "Level6") player.level7 = true;
                else if (SceneManager.GetActiveScene().name == "Level7") player.level8 = true;
                else if (SceneManager.GetActiveScene().name == "Level8") player.level9 = true;
                else if (SceneManager.GetActiveScene().name == "Level9") player.level10 = true;
                else if (SceneManager.GetActiveScene().name == "Level10") player.level11 = true;
                else if (SceneManager.GetActiveScene().name == "Level11") player.level12 = true;
                else if (SceneManager.GetActiveScene().name == "Level12") player.survival = true;

                SaveSystem.SavePlayer(player);

                GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().CancelInvoke();
                StartCoroutine(DestroyLauncher());
                Started = true;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                x -= Time.deltaTime * RotationSensitivity;
                transform.rotation = Quaternion.Euler(0, x, 0);
            }
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                x += Time.deltaTime * RotationSensitivity;
                transform.rotation = Quaternion.Euler(0, x, 0);
            }
            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && !GameObject.Find("Canvas").GetComponent<PauseMenuScript>().IsPaused)
            {

                if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == RedBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = RedBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }
                else if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == BlueBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = BlueBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }
                else if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == YellowBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = YellowBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }
                else if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == GreenBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = GreenBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }
                else if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == PurpleBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = PurpleBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }
                else if (GameObject.Find("NextBiocodePrefab(Clone)").GetComponent<MeshRenderer>().sharedMaterial == OmniBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ShootBiocode.GetComponent<MeshRenderer>().sharedMaterial = OmniBiocodePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                }

                this.GetComponent<AudioSource>().Play();
                Instantiate(ShootBiocode, GameObject.Find("BiocodeLauncherPrefab/Shooter").transform.position, transform.rotation);

                Destroy(GameObject.Find("NextBiocodePrefab(Clone)"));
                NextBiocode.transform.position = posNext;
                Instantiate(NextBiocode);
            }
        }
    }
}
