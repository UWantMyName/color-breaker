﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextBiocodeScript : MonoBehaviour
{
    public Material RedPrefab;
    public Material BluePrefab;
    public Material YellowPrefab;
    public Material GreenPrefab;
    public Material PurplePrefab;
    public Material OmniPrefab;

    // Use this for initialization
	void Start ()
    {
        if (SceneManager.GetActiveScene().name == "Level10" || SceneManager.GetActiveScene().name == "Level11" || SceneManager.GetActiveScene().name == "Level12")
        {
            int x = Random.Range(0, 600);

            if (x % 6 == 0 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Red")) this.GetComponent<Renderer>().sharedMaterial = RedPrefab;
            else if (x % 6 == 1 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Blue")) this.GetComponent<Renderer>().sharedMaterial = BluePrefab;
            else if (x % 6 == 2 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Yellow")) this.GetComponent<Renderer>().sharedMaterial = YellowPrefab;
            else if (x % 6 == 3 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Green")) this.GetComponent<Renderer>().sharedMaterial = GreenPrefab;
            else if (x % 6 == 4 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Purple")) this.GetComponent<Renderer>().sharedMaterial = PurplePrefab;
            else if (x % 6 == 5 && GameObject.FindGameObjectWithTag("Biocode").name.Contains("Black")) this.GetComponent<Renderer>().sharedMaterial = OmniPrefab;
        }
        else
        {
            int x = Random.Range(0, 100);

            if (x % 5 == 0) this.GetComponent<Renderer>().sharedMaterial = RedPrefab;
            else if (x % 5 == 1) this.GetComponent<Renderer>().sharedMaterial = BluePrefab;
            else if (x % 5 == 2) this.GetComponent<Renderer>().sharedMaterial = YellowPrefab;
            else if (x % 5 == 3) this.GetComponent<Renderer>().sharedMaterial = GreenPrefab;
            else if (x % 5 == 4) this.GetComponent<Renderer>().sharedMaterial = PurplePrefab;
        }
    }
}
