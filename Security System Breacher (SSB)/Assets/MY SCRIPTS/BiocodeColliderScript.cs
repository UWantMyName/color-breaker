﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiocodeColliderScript : MonoBehaviour
{
    public Rigidbody rb;

    private void OnCollisionEnter(Collision collision)
    {
        GameObject me = rb.gameObject;
        GameObject WhatIHit = collision.gameObject;
        string redPrefab = "RedBiocodePrefab";
        string bluePrefab = "BlueBiocodePrefab";
        string greenPrefab = "GreenBiocodePrefab";
        string yellowPrefab = "YellowBiocodePrefab";
        string purplePrefab = "PurpleBiocodePrefab";

        if (WhatIHit.name == redPrefab && me.GetComponent<Renderer>().sharedMaterial != WhatIHit.GetComponent<Renderer>().sharedMaterial)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            Destroy(me);
        }

        if (WhatIHit.name == bluePrefab && me.GetComponent<Renderer>().sharedMaterial != WhatIHit.GetComponent<Renderer>().sharedMaterial)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            Destroy(me);
        }

        if (WhatIHit.name == yellowPrefab && me.GetComponent<Renderer>().sharedMaterial != WhatIHit.GetComponent<Renderer>().sharedMaterial)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            Destroy(me);
        }

        if (WhatIHit.name == greenPrefab && me.GetComponent<Renderer>().sharedMaterial != WhatIHit.GetComponent<Renderer>().sharedMaterial)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            Destroy(me);
        }

        if (WhatIHit.name == purplePrefab && me.GetComponent<Renderer>().sharedMaterial != WhatIHit.GetComponent<Renderer>().sharedMaterial)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            Destroy(me);
        }
    }

}
