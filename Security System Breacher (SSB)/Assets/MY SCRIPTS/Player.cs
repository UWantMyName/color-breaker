﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxhealth;
    public int damage;
    public int coins;
    public bool level1;
    public bool level2;
    public bool level3;
    public bool level4;
    public bool level5;
    public bool level6;
    public bool level7;
    public bool level8;
    public bool level9;
    public bool level10;
    public bool level11;
    public bool level12;
    public bool survival;

    public Player LoadPlayer(PlayerData data)
    {
        Player player = new Player();

        player.level1 = data.level1;
        player.level1 = data.level2;
        player.level1 = data.level3;
        player.level1 = data.level4;
        player.level1 = data.level5;
        player.level1 = data.level6;
        player.level1 = data.level7;
        player.level1 = data.level8;
        player.level1 = data.level9;
        player.level1 = data.level10;
        player.level1 = data.level11;
        player.level1 = data.level12;
        player.survival = data.survival;

        player.damage = data.damage;
        player.maxhealth = data.maxhealth;
        player.coins = data.coins;

        return player;
    }

    public Player CreatePlayer()
    {
        Player player = new Player();

        player.level1 = true;
        player.level2 = false;
        player.level3 = false;
        player.level4 = false;
        player.level5 = false;
        player.level6 = false;
        player.level7 = false;
        player.level8 = false;
        player.level9 = false;
        player.level10 = false;
        player.level11 = false;
        player.level12 = false;
        player.survival = false;

        player.damage = 1;
        player.maxhealth = 5;
        player.coins = 0;

        return player;
    }
}
