﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTumblerScript : MonoBehaviour
{
    public int BiocodesLeft = 3;
    public bool ErrorRotation = false;
    private bool IsStarted = false;
    private bool IsStartedError = false;

    public IEnumerator RotateObject(float angle, Vector3 axis, float inTime)
    {
        // calculate rotation speed
        float rotationSpeed = angle / inTime;

        while (true)
        {
            // save starting rotation position
            Quaternion startRotation = transform.rotation;

            float deltaAngle = 0;

            // rotate until reaching angle
            while (deltaAngle < angle)
            {
                deltaAngle += rotationSpeed * Time.deltaTime;
                deltaAngle = Mathf.Min(deltaAngle, angle);

                transform.rotation = startRotation * Quaternion.AngleAxis(deltaAngle, axis);

                yield return null;
            }
            // delay here
            yield return new WaitForSeconds(2);
        }
    }

    public IEnumerator RotateError(float angle, Vector3 axis, float inTime)
    {
        // calculate rotation speed
        float rotationSpeed = angle / inTime;

        while (true)
        {
            // save starting rotation position
            Quaternion startRotation = transform.rotation;

            float deltaAngle = 0;

            // rotate until reaching angle
            while (deltaAngle < angle)
            {
                deltaAngle += rotationSpeed * Time.deltaTime;
                deltaAngle = Mathf.Min(deltaAngle, angle);

                transform.rotation = startRotation * Quaternion.AngleAxis(deltaAngle, axis);

                yield return null;
            }
            // delay here
            ErrorRotation = false;
            IsStartedError = false;
            yield return new WaitForSeconds(2);
            StopAllCoroutines();
            IsStarted = false;
        }   
    }

    private void Start()
    {
        StartCoroutine(RotateObject(120, Vector3.up, 0.2f));
        ErrorRotation = false;
        IsStarted = true;
        IsStartedError = false;
    }

    private void UpdateRotation()
    {
        float y = this.transform.rotation.y;

        while (y > 360) y -= 360;

        if (y < 0) y *= -1;

        if (0f <= y && y <= 120f)
        {
            float d1, d2;

            d1 = y - 0;
            d2 = 120f - y;
            if (d2 < d1) y = 120f;
            else y = 0f;
        }
        else if (120f <= y && y <= 240f)
        {
            float d1, d2;

            d1 = y - 120f;
            d2 = 240f - y;
            if (d2 < d1) y = 240f;
            else y = 120f;
        }
        else if (240f <= y && y <= 360f)
        {
            float d1, d2;

            d1 = y - 240f;
            d2 = 360f - y;
            if (d2 < d1) y = 360f;
            else y = 240f;
        }

        var rot = transform.rotation;
        rot.y = y;
        transform.rotation = rot;
    }

    private void Update()
    {
        if (BiocodesLeft == 0)
        {
            StopAllCoroutines();
        }
        else
        {
            if (ErrorRotation)
            {
                if (!IsStartedError)
                {
                    StopAllCoroutines();
                    IsStartedError = true;
                    StartCoroutine(RotateError(2387, Vector3.up, 3f));
                }
            }
            else
            {
                if (!IsStarted)
                {
                    IsStarted = true;
                    StartCoroutine(RotateObject(120, Vector3.up, 0.2f));
                }
            }
        }
    }
}
