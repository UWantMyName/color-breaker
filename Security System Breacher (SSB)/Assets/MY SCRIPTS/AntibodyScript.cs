﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntibodyScript : MonoBehaviour
{
    public GameObject DestroyEffect;

    public float Health;
    public bool shield = false;
    public int TimeLapse = 0;
    public int ShieldTime = 100;
    public float duration = 3f;
    public bool stop = false;

    public float speed = 0.5f;

    public GameObject Shield;

    public IEnumerator KillAntibody()
    {
        var efect = Instantiate(DestroyEffect, transform.position, transform.rotation);
        efect.transform.parent = gameObject.transform;
        GetComponent<BoxCollider>().enabled = false; // so it does not decrease the health more than once
        speed = 0; // so it cannot move
        this.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);

        bool added = false;
        if (!added && gameObject.name == "AntiBody1Prefab(Clone)") { GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().coins += 1; added = true; }
        else if (!added && gameObject.name == "AntiBody2Prefab(Clone)") { GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().coins += 1; added = true; }

        if (GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned > 0)
            GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned--;

        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "ShootBiocodePrefab(Clone)")
        {
            Destroy(collision.gameObject);
            if (!shield)
            {
                transform.position += new Vector3((100 * (-transform.forward.x * Time.deltaTime * speed)), 0, 0);
                Health -= GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().damage;
                if (Health >= 1)
                {
                    Instantiate(Shield, transform.position, Quaternion.identity);
                    TimeLapse = 0;
                }
                if (Health <= 0)
                {
                    StartCoroutine(KillAntibody());
                }
                else shield = true;
            }
            else TimeLapse = 0;
        }
        
        if (collision.gameObject.tag == "TumblerBiocode")
        {
            Destroy(gameObject);
            if (GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned > 0) GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().ABSpawned--;
        }
    }

    private void Start()
    {
        if (GameObject.Find("ShieldPrefab(Clone)")) Destroy(GameObject.Find(Shield.gameObject.name + "ShieldPrefab(Clone)"));
    }

    private void FixedUpdate()
    {
        GameObject launcher = GameObject.Find("BiocodeLauncherPrefab");

        if (launcher != null) transform.LookAt(launcher.transform);

        if (shield)
        {
            if (TimeLapse < ShieldTime) TimeLapse++;
            else
            {
                shield = false;
                TimeLapse = 0;
                Destroy(GameObject.Find(Shield.name + "(Clone)"));
            }
        }
        else
        {
            if (launcher.activeInHierarchy) transform.position += transform.forward * Time.deltaTime * speed;
        }   
    }
}
