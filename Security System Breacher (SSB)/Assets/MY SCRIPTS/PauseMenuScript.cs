﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    public bool IsPaused = false;
    public GameObject PauseMenuUI;
    public GameObject SucceedMenuUI;
    public GameObject TimeElapsedUI;
    //private bool ChangeScene = false;
	
    private IEnumerator Wait()
    {
        float x;
        float sec;
        float min = 0;

        GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().CancelInvoke();

        x = Time.timeSinceLevelLoad;
        while (x >= 60)
        {
            x -= 60;
            min++;
        }
        sec = x;

        if (min < 10.0f)
        {
            if (sec < 10.0f) TimeElapsedUI.GetComponent<Text>().text = "Time elapsed:      0" + min + ":0" + (int)sec;
            else TimeElapsedUI.GetComponent<Text>().text = "Time elapsed:      0" + min + ":" + (int)sec;
        }
        else TimeElapsedUI.GetComponent<Text>().text = "Time elapsed:      " + min + ":" + (int)sec;

        Time.timeScale = 0;
        SucceedMenuUI.SetActive(true);
        // I have to save everything to the system
        
        PlayerData player = SaveSystem.LoadPlayer();
        player.damage = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().damage;
        player.coins = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().coins;
        player.maxhealth = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.maxhealth; // PLAYER DATA! NOT THE ACTUAL PLAYER!!!
        if (SceneManager.GetActiveScene().name == "Level1") player.level2 = true;
        if (SceneManager.GetActiveScene().name == "Level2") player.level3 = true;
        if (SceneManager.GetActiveScene().name == "Level3") player.level4 = true;
        if (SceneManager.GetActiveScene().name == "Level4") player.level5 = true;
        if (SceneManager.GetActiveScene().name == "Level5") player.level6 = true;
        if (SceneManager.GetActiveScene().name == "Level6") player.level7 = true;
        if (SceneManager.GetActiveScene().name == "Level7") player.level8 = true;
        if (SceneManager.GetActiveScene().name == "Level8") player.level9 = true;
        if (SceneManager.GetActiveScene().name == "Level9") player.level10 = true;
        if (SceneManager.GetActiveScene().name == "Level10") player.level11 = true;
        if (SceneManager.GetActiveScene().name == "Level11") player.level12 = true;
        if (SceneManager.GetActiveScene().name == "Level12") player.survival = true;
        SaveSystem.SavePlayer(player);

        yield return new WaitForSecondsRealtime(3);
        StopAllCoroutines();
        SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsPaused) Resume();
            else Pause();
        }

        if (GameObject.Find("BiocodesLeftPrefab").GetComponent<BiocodeCounter>().BiocodesLeft == 0)
        {
            StartCoroutine(Wait());
        }
	}

    public void Resume()
    {
        InvokeRepeating("GameObject.Find(" + "AntibodySpawnerPrefab" + ").GetComponent<AntibodySpawner>().SpawnAntibody", 2.0f, 3.0f);
        InvokeRepeating("GameObject.Find(" + "AntibodySpawnerPrefab" + ").GetComponent<AntibodySpawner>().SpawnCoin", 2.0f, 10.0f);
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        IsPaused = false;
    }

    public void Pause()
    {
        GameObject.Find("AntibodySpawnerPrefab").GetComponent<AntibodySpawner>().CancelInvoke();
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        IsPaused = true;
    }

    public void QuitButton_Click()
    {
        // I have to save everything to the system
        // I have to save everything to the system
        PauseMenuUI.SetActive(false);
        Player player = new Player();
        player.LoadPlayer(SaveSystem.LoadPlayer());
        player.damage = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().damage;
        player.coins = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().coins;
        player.maxhealth = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.maxhealth; // PLAYER DATA! NOT THE ACTUAL PLAYER!!!
        player.level1 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level1;
        player.level2 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level2;
        player.level3 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level3;
        player.level4 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level4;
        player.level5 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level5;
        player.level6 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level6;
        player.level7 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level7;
        player.level8 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level8;
        player.level9 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level9;
        player.level10 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level10;
        player.level11 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level11;
        player.level12 = GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.level12;
        player.survival= GameObject.Find("BiocodeLauncherPrefab").GetComponent<LauncherScript>().data.survival;
        SaveSystem.SavePlayer(player);

        SceneManager.LoadScene(1);
    }
}
